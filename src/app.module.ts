import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CoffeesController } from './coffees/coffees.controller';
import { CoffeesService } from './coffees/coffees.service';
import { CoffeesModule } from './coffees/coffees.module';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UsersModule } from './users/users.module';
import {  UsersService } from './users/users.service';
import { UsersEntity } from './users/user';

@Module({
  imports: [CoffeesModule,
  TypeOrmModule.forRoot({
    type: 'mysql',
    host: 'localhost',
    port: 5432,
    username: 'root',
    password: '',
    database: 'test_nest',
   entities: [UsersEntity],
    synchronize: true
  }),
  UsersModule],
  controllers: [AppController ],
  providers: [AppService, UsersService],
})
export class AppModule {}
