import { Injectable } from '@nestjs/common';
import { UpdateCoffeeDto } from './dto/update-coffee.dto';
import { Coffee } from './entinities/coffee.entinty';

@Injectable()
export class CoffeesService {
    private coffees= [] = [
        {
            id: 1,
            name: 'hzbor',
            brand: 'majd',
            flavors: ['choco', 'vanilla']
        },
        {
            id: 2,
            name: 'hzbor',
            brand: 'majd',
            flavors: ['choco', 'vanilla']
        }
    ];

    findAll(){
        return this.coffees;
    }

    findOne(id : string){
        return this.coffees.find(item => item.id === +id);
    }

    create(createCoffeeDto: any){
        return this.coffees.push(createCoffeeDto);
    }

    update(id: string, _updateCoffeeDto: any){
        const existingCoffee = this.findOne(id);
        //return existingCoffee;
        if (existingCoffee){
           
           return 'done';
        }
        else{
            
            return 'not done';
            
        }
    }
    remove(id: string){
        const existingCoffee =  this.coffees.findIndex(item => item.id === +id);;
        if(existingCoffee >=0){
  this.coffees.splice(existingCoffee,1);
        } 

    }

}
