import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Res } from '@nestjs/common';
import { response } from 'express';
import { UsersEntity } from './user';
import {  UsersService } from './users.service';

@Controller('users')
export class UsersController {
    constructor(private userService: UsersService){

    }

    @Get()
    async findAll(@Res() res:Response){
        const Response = await this.userService.findAll();
        return response;
        // res.status(HttpStatus.OK).json({payload: response})
    }

    // @Post()
    // async creat(@Body() createUserDto: UserEntity, @Res() res :Response){
    //     const response = await this.userService.create(createUserDto)
    // }

    // @Put()
    // async update(@Param() id: number, @Body() createUserDto:UserEntity, @Res() res:Response){
    //     const response = await this.userService.update(id, createUserDto)
    // }

    // @Delete()
    // async delete(@Body() id:number , @Res() res:Response){
    //    const response = await this.userService.remove(id);
    //    return response;
    // }

    
        
    
}
