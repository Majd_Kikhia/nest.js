import { Column, PrimaryGeneratedColumn } from "typeorm";
import { Entity } from "typeorm/decorator/entity/Entity";

@Entity()
export class UsersEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name:string   

    @Column()
    email:string  
}
