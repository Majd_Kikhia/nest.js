import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Column, Entity , PrimaryGeneratedColumn, Repository } from 'typeorm';
import { UsersEntity } from './user';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(UsersEntity)
        private userRepositry: Repository<UsersEntity>
    ) { }

    findAll(): Promise<UsersEntity[]>{
        return this.userRepositry.find()
    }

    // findOne(isd:number ) : Promise<UserEntity>{
    //     return this.userRepositry.findOne(isd)
    // }

    // create(user: UserEntity): Promise<UserEntity>{
    //     return this.userRepositry.save(user)
    // }



    // async update(id:number , user: UserEntity){
    //     await this.userRepositry.update(id,user)
    // }

    // async remove (id:number) : Promise<void>{
    //     await this.userRepositry.delete(id);
    // }


    
}

