import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersEntity } from './user';
import {  UsersService } from './users.service';

@Module({
   imports:[ TypeOrmModule.forFeature([UsersEntity])],
  controllers: [ ],
  providers: [UsersService],
})
export class UsersModule {}

